import { HpMedFormsPage } from './app.po';

describe('hp-med-forms App', () => {
  let page: HpMedFormsPage;

  beforeEach(() => {
    page = new HpMedFormsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
