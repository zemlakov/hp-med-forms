import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { TemplateFormService } from '../services/template-form/template-form.service';
import { IncidentService } from '../services/incident/incident.service';
import { CurrentFormDataService } from '../services/current-form-data/current-form-data.service';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit {
  isFormState: boolean = false;
  nav: any;

  selectedForm: any;

  constructor(
    public incident: IncidentService,
    public currentFormSevice: CurrentFormDataService,
    private route: ActivatedRoute,
    private router: Router,
    public formTempalte: TemplateFormService
  ) {
    // Load template in scope
      this.formTempalte.loadCurrentTemplate();
   }

  ngOnInit() {

    // Watch on app state: if form - hide user and show form tools
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isFormState = this.isForm(this.router.routerState.snapshot.url);

        // Helper for dropdown form selector
        if ( this.isFormState) {
          this.selectedForm = this.formTempalte.getRenderFormName(this.currentFormSevice.currentFormID);
        }

        console.log(this.isFormState);

      }
    });

    // Load list of forms for NavigationEnd
    this.nav = this.formTempalte.getNavItems();
  }

  isForm(link: String) {
    if (link.substr(0, 5) === '/form') {
      return true;
    }else {
      return false;
    }
  }

  onSelect(item: any) {
    this.currentFormSevice.currentFormID = item.id;

    if (this.incident.currentIncidentStatus == 'new') {
      this.currentFormSevice.currentFormStatus = 'new';
    }else {
      // Set current form status
      if (this.incident.getFormData(this.incident.currentIncidentID, item.id)) {
        this.currentFormSevice.currentFormStatus = 'saved';
        let formData = this.incident.getFormData(this.incident.currentIncidentID, item.id);
        this.currentFormSevice.setFormData(formData);
      }else{
        this.currentFormSevice.currentFormStatus = 'new';
      }
    }

    // Go to specific form
    this.router.navigate(['/form', item.id]);
  }

}
