import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import * as $ from 'jquery';
import { IncidentService } from './services/incident/incident.service';
import { CurrentFormDataService } from './services/current-form-data/current-form-data.service';
import { TemplateFormService } from './services/template-form/template-form.service';
import { CustomerDataService } from './services/customer-data/customer-data.service';
import { UpdateService } from './services/update/update.service';
import { DynamicTypeBuilder } from './form-template/form.builder';

import { AppComponent } from './app.component';
import { FormTemplateComponent } from './form-template/form-template.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { HomeComponent } from './home/home.component';
import { FormToolsComponent } from './form-tools/form-tools.component';
import { FormListComponent } from './form-list/form-list.component';
import { UpdateComponentComponent } from './update-component/update-component.component';

import { ModalModule } from 'ngx-bootstrap/modal';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';


const appRoutes: Routes = [
  { path: 'form/:id',      component: FormTemplateComponent },
  { path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  { path: '**', component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    FormTemplateComponent,
    AppHeaderComponent,
    HomeComponent,
    FormToolsComponent,
    FormListComponent,
    UpdateComponentComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    NKDatetimeModule
  ],
  providers: [DynamicTypeBuilder, CurrentFormDataService, TemplateFormService, CustomerDataService, IncidentService, UpdateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
