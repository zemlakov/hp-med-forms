import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';

import { IncidentService } from '../services/incident/incident.service';
import { CurrentFormDataService } from '../services/current-form-data/current-form-data.service';

@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.scss']
})
export class FormListComponent implements OnInit {

  selectedIncident = 'all';
  incidentList = [];

  visibleList = [];

  constructor(
    public incident: IncidentService,
    public currentFormSevice: CurrentFormDataService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    // Set initial properties on component init
    this.incidentList = this.incident.getIncidentsList();
    this.visibleList = JSON.parse(JSON.stringify(this.incidentList)); // Creating another instance to break refference to the source
  }

  getSheetStatus(type: string, list: any) {

    let founded = false;

    list.forEach(element => {
      if (element[type]) {
        founded = true;
      }
    });

    return founded;
  }

  onIncidentChange(val) {
    if (val !== 'all') {
      this.incidentList.forEach( item => {
        if (item.id == val) {
          this.visibleList = [item];
        }
      });
    }else {
      this.visibleList = JSON.parse(JSON.stringify(this.incidentList));
    }
  }

  // Check, is selected incident active
  isFormActive() {
    let status = false;

    if (this.selectedIncident !== 'all') {
      this.incidentList.forEach( item => {
        if (String(item.id) === this.selectedIncident) {
          status = item.status;
        }
      });
    }

    return status;
  }

  // Create new incident
  createNewIncident() {

    // Set incident properties for new item
    this.incident.currentIncidentID = false;
    this.incident.currentIncidentStatus = 'new';

    // Go to 'Stammdaten' first and set init form/incident data
    this.currentFormSevice.setFormId('sd');
    this.currentFormSevice.currentFormStatus = 'new';
    this.router.navigate(['/form', 'sd']);

  }

  // Delet existed incident
  deleteIncidentItem(id: Number) {
    // remove from incident's storage
    this.incident.deleteincident(id);

    // Reinit properties on component
    this.incidentList = this.incident.getIncidentsList();
    this.visibleList = JSON.parse(JSON.stringify(this.incidentList));
  }

  // View existing incident
  viewIncident(formId: string, incidentId: number, active: boolean) {
    if (active) {
      // Set incident properties
      this.incident.currentIncidentID = incidentId;
      this.incident.currentIncidentName = this.incident.getIncidentName(incidentId);
      this.incident.currentIncidentStatus = 'saved';

      // Go to specific form and set init form/incident data
      this.currentFormSevice.currentFormStatus = 'saved';
      this.currentFormSevice.currentFormID = formId;
      let formData = this.incident.getFormData(incidentId, formId);
      this.currentFormSevice.setFormData(formData);

      this.router.navigate(['/form', formId]);
    }

  }

  // Add new form to the existing incident
  addFormToIncident(formId: string, incidentId: number) {
    console.log('Form is: ' + formId + ' Incident ID: ' + incidentId);

    // Set incident properties
    this.incident.currentIncidentID = incidentId;
    this.incident.currentIncidentName = this.incident.getIncidentName(incidentId);
    this.incident.currentIncidentStatus = 'saved';

    // Go to specific form and set init form/incident data
    this.currentFormSevice.currentFormStatus = 'new';
    this.currentFormSevice.currentFormID = formId;

    this.router.navigate(['/form', formId]);
  }
}
