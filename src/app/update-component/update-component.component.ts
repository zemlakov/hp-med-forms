import { Component, OnInit } from '@angular/core';

import { UpdateService } from '../services/update/update.service';
import { TemplateFormService } from '../services/template-form/template-form.service';

@Component({
  selector: 'app-update-component',
  templateUrl: './update-component.component.html',
  styleUrls: ['./update-component.component.scss']
})
export class UpdateComponentComponent implements OnInit {

  public templateStatus = null; // In case if current tpl in local storage doesn't match template version on server
  public currentTemplateVersion;

  constructor(
    public update: UpdateService,
    public template: TemplateFormService
    ) { }

  ngOnInit() {
      // Set curren template version or download if empty
      if (this.template.getTemplateVersion()) {
        this.currentTemplateVersion = this.template.getTemplateVersion();
        this.checkTemplates();
      }else {
        this.updateTemplates();
      }
  }

  // Check templates status
  public checkTemplates() {
      // Update specific user with new data
      this.update.getTemplatesVersion().subscribe((version) => {
        if (version) {

          // If the same - set status to true, else -false
          if (version == this.currentTemplateVersion) {
            this.templateStatus = true;
          } else {
            this.templateStatus = false;
          }

        }
      });
  }

  // Update templates
  public updateTemplates() {
    console.log('Update fired!');

    // get templates from server
    this.update.getTemplates().subscribe((tpl) => {
      if (tpl) {
        // and store it local storege to: currentTemplate
        this.template.saveTemplate(tpl);
        this.template.loadCurrentTemplate();
        this.currentTemplateVersion = this.template.getTemplateVersion();
        this.checkTemplates();
      }
    });

  }

}
