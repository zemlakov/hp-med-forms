import { Component, OnInit, ViewChild  } from '@angular/core';

import { IncidentService } from '../services/incident/incident.service';
import { CurrentFormDataService } from '../services/current-form-data/current-form-data.service';
import { TemplateFormService } from '../services/template-form/template-form.service';

import { ModalDirective } from 'ngx-bootstrap/modal';

declare var pdfMake: any;
declare var window: any;

@Component({
  selector: 'app-form-tools',
  templateUrl: './form-tools.component.html',
  styleUrls: ['./form-tools.component.scss']
})
export class FormToolsComponent implements OnInit {

  @ViewChild('saveModal') public saveModal: ModalDirective;

  constructor(
    public incidents: IncidentService,
    public currentFormSevice: CurrentFormDataService,
    public formTempalte: TemplateFormService
  ) {}

  ngOnInit() {
  }

  // Proccess entire template for smaller content blocks
  processTemplate(tpl: any, baseData: boolean) {

    // Print content consist of content blocks {} with tables inside
    let printContent = [];

    if (tpl.template.length > 0) {
      tpl.template.forEach( (contentBlock) => {
        let processedBlock = this.processBlock(contentBlock, tpl, baseData);
        if (processedBlock) {
          printContent.push(processedBlock);
        }
      });
    }else  {
      console.log('Print template is empty or consist with wrong data!');
    }

    return printContent;

  }

  // Process content block of template
  processBlock (block: any, tpl: any, baseData: boolean) {

    let processedBlock = [];

    if (this.isBlockVisible(block, baseData)) {

      // If block is table built from array
      if (block['arrayRow']) {
        let arrayRows = this.getPropertyValue(block['arrayRow'], baseData);
        let rowTpl;
        let bodyArray =[];

        // set row template - 0 line of table
        if (block.table.body) {
          rowTpl = block.table.body[0];
          console.log('row tpl is: '+JSON.stringify(rowTpl));
          
        }

        // Check is there template described and pass through it's rows
        arrayRows.forEach( (rowItem) => {
          let processedRow = [];
          // Proces each template row item
          rowTpl.forEach( (tplItem) => {
            let item = tplItem;
            // If item not empty - add value
            if (rowItem[tplItem.text] !== undefined) {
              item['text'] = rowItem[tplItem.text];
              console.log('Ite, is: '+JSON.stringify(item));
              
              processedRow.push(item);
            }
          });
          console.log('Processed row is: '+ JSON.stringify(processedRow));
          
          bodyArray.push(processedRow);
        });

        block.table.body = bodyArray;
        processedBlock = block;

      } else {
        // Check is there template described and pass through it's rows
        if (block.table.body) {
          block.table.body.forEach( (row, rowIndex) => {

            // and fill it with data from currentFormData
            row.forEach( (col, colIndex) => {
              if (col.text) {
                let id = col.text;
                block.table.body[rowIndex][colIndex].text = this.setPropertyValue(id, tpl, baseData);
              }
            });

          });

          processedBlock = block;
        }
      }

    }else {
      let processedBlock = false;
    }

    return processedBlock;
  }

  // Check the visibility of content block (check in data schema value)
  isBlockVisible(block: any, baseData: boolean) {

    let visible = false;

    if (block['visibleIf']) {

      let property = block['visibleIf'];

      visible = this.getPropertyValue(property, baseData);

      // For arrays
      if (Array.isArray(visible) && visible.length > 0) {
        visible = true;
      }


    } else {
      // If not such property - block is visible by default
      visible = true;
    }

    return visible;
  }

  // Get property value from schema () by it's 'root' in string: ex. "over_the_device.modules.rm"
  getPropertyValue (property: string, baseData: boolean):any {
    let propertyValue = false;

      // get value from currentFormData
      let currentData;
      // In case of 'baseData: true' - using stored form data from incident storage, else - using currentFormData
      if (baseData) {
        let incidentId = this.incidents.currentIncidentID;
        let baseDataFormData = this.incidents.getFormData(incidentId, 'sd');

        currentData = JSON.parse(JSON.stringify(baseDataFormData));
      } else {
        currentData = JSON.parse(JSON.stringify(this.currentFormSevice.currentFormData));
      }

      // Split the string to array of keys
      console.log('Prop is: ' + JSON.stringify(property));

      let arrayOfRoot = property.split('.');

      console.log('Array of prps: ' + JSON.stringify(arrayOfRoot));

      arrayOfRoot.forEach(element => {
        if (currentData[element] !== undefined) {
          currentData = currentData[element];
          propertyValue = currentData;
        }
      });

      console.log('Value is : ' + JSON.stringify(propertyValue));

      return propertyValue;
  }

  setPropertyValue(propertyName, tpl: any, baseData: boolean) {
    // get value from currentFormData or saved incident (for 'sd')
    let value = null;
    if (tpl.properties[propertyName]) {
      value = this.getPropertyValue(tpl.properties[propertyName].relatedModel, baseData);
    }

    let propertyValueProcessed;

    // Method for filling text field when value not empty
    let processValuePropertyTemplate = (propValue: any) => {

      let template = JSON.parse(JSON.stringify(tpl.properties[propertyName].value_template));

      template.forEach( (item, index) => {
        if (item === 'propertyValue') {
          template[index] = propValue;
        }
      });

      return template.join('');
    };

    // get propery template from printContentSpecification and will it's template with data from currentFormData
    //
    // If value is defined and not empty use value_template of property
    if (value) {
      propertyValueProcessed = processValuePropertyTemplate(value);
    }else if (value !== null) {
      propertyValueProcessed = tpl.properties[propertyName].empty_template;
    }else {
      propertyValueProcessed = propertyName;
    }

    return propertyValueProcessed;
  }


  // Method which callin when 'pdf' or 'print' button clicked
  processForm(type: String) {
    // Set Observable value to submit current form
    this.currentFormSevice.formStateSource.next(true);

    // Check value of current form validation
    if (this.currentFormSevice.currentFormValid) {
      this.generatePdfFile(type);
    } else {
      alert('Check the form!');
    }
  }

  // Method for generating PDF or Printing
  generatePdfFile(output: String) {

    // Set SD (base data) template as document parent tpl (here is no dynamic processed values)
    let docTemplate = this.formTempalte.getPrintLayout('sd');

    // **Fill document with base data dynamic  part (print content)
    let baseContent = this.formTempalte.getPrintContent('sd');
    let baseContentData = this.processTemplate(baseContent, true);

    baseContentData.forEach( (contentBlock) => {
      docTemplate.content.push(contentBlock);
    });

    // **Fill document with currentForm data dynamic  part (print content)
    let tplContent = this.formTempalte.getPrintContent(this.currentFormSevice.currentFormID);
    let tplContentData = this.processTemplate(tplContent, false);

    tplContentData.forEach( (contentBlock) => {
      docTemplate.content.push(contentBlock);
    });

    //console.log(JSON.stringify(docTemplate));
    // Add page preak method
    docTemplate['pageBreakBefore'] = function(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
      return currentNode.startPosition.top >= 800;
    };

    if (output === 'pdf') {
      // File name 'currentFormId'
      pdfMake.createPdf(docTemplate).download(this.currentFormSevice.currentFormID + '.pdf');
    }else {
      pdfMake.createPdf(docTemplate).print();
    }
  }

  /*sendPdfFile() {

    // Unbin from orginal Instance reference
    let docTemplate = JSON.parse(JSON.stringify(this.prtintLayoutSpecification));

    let contentData = this.processTemplate();

    contentData.forEach( (contentBlock) => {
      docTemplate.content.push(contentBlock);
    });

    const pdfDocGenerator = pdfMake.createPdf(docTemplate);

    pdfDocGenerator.getBase64((dataUrl) => {

      const email = 'zemlakov@gmail.com';
      const subject = 'Test';
      const emailBody = 'Hi Sample,';
      let attach = 'index.html';

      window.location.href = 'mailto:' + email + '?subject=' + subject + '&body=' + emailBody + '&attach=' + attach;

    });
  }*/

  // Method which callin when 'remove' button clicked
  clearForm() {
    this.incidents.delteCurrentForm();
  }

  // Method which callin when 'save' button clicked
  saveForm() {
    if (this.incidents.currentIncidentStatus === 'new') {
      this.saveModal.show();
    }else {
      console.log('Update form in incident fired!!!');
      // Save current form to the new Incident
      this.incidents.saveCurrentForm();
    }
  }

  // Helper method, which used if current incident not saved
  confirmSavigIncident(name: string) {
    // Set current incident name in Incident service
    this.incidents.currentIncidentName = name;

    // Save new incident
    this.incidents.saveNewIncident();

    // Save current form to the new Incident
    this.incidents.saveCurrentForm();

    // And close modal
    this.saveModal.hide();
  }

  hideSaveModal(): void {
    this.saveModal.hide();
  }

}
