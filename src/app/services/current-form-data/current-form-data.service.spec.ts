import { TestBed, inject } from '@angular/core/testing';

import { CurrentFormDataService } from './current-form-data.service';

describe('CurrentFormDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CurrentFormDataService]
    });
  });

  it('should ...', inject([CurrentFormDataService], (service: CurrentFormDataService) => {
    expect(service).toBeTruthy();
  }));
});
