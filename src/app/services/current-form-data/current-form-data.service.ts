/**
 * Service which we use to share current form data between "form-template" component and "generate-pdf" component
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CurrentFormDataService {

  public currentFormData: any;              // Object with current form data. Populates initialy from render form data schema
  public currentFormID: any;                // Id of the form (sd, ws ...). Sets from render data schema
  public currentFormStatus: any;            // Statuses: new, edited, saved
  public currentFormValid: any = false;

  // Observable source
  public clearStateSource = new Subject<boolean>();
  public formStateSource = new Subject<boolean>();  // Valid and sumitted (true), else - (false)

  // Observable stream
  clearState$ = this.clearStateSource.asObservable();
  formState$ = this.formStateSource.asObservable();

  constructor() {
    // Set default clear state
    this.clearStateSource.next(false);
    this.formStateSource.next(false);
  }

  setFormData(data: any) {
    this.currentFormData = data;
  }

  setFormId(id: any) {
    this.currentFormID = id;
  }

}
