import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';

@Injectable()
export class CustomerDataService {

 public deviceData: any;
 public customerData: any;

  constructor() {

    this.deviceData = {
      "createdAt": "12-04-2017",
      "data": [{
          "id": "0001",
          "name": "Autoklav"
        },
        {
          "id": "0002",
          "name": "Another Autoklav"
        }
      ]
    };

    this.customerData = {
      "createdAt": "12-04-2017",
      "data": [{
        "id": "0001",
        "name": "Bundesamt für Wehrtechnik und Beschaffung",
        "street": "Krenn 2",
        "zip": "22049",
        "city": "Hamburg",
        "appeal": "Frau",
        "contact": "Menzel"
      },
      {
        "id": "0002",
        "name": "qqq",
        "street": "Lesserstr. 182",
        "zip": "22049",
        "city": "berlin",
        "appeal": "Herr",
        "contact": "Martin"
      }
      ]
    };

  }

  public getCustomerData(token: string, type: string): Observable<any> {
      let query = new RegExp(token, 'ig');

      return Observable.of(
        this.customerData['data'].filter((customerItem: any) => {
          return query.test(customerItem[type]);
        })
      );
  }

  public getDeviceData(token: string, type: string): Observable<any> {
      let query = new RegExp(token, 'ig');

      return Observable.of(
        this.deviceData['data'].filter((deviceItem: any) => {
          return query.test(deviceItem[type]);
        })
      );
  }

}
