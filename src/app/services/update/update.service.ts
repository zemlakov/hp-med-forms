import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class UpdateService {

  private apiUrl = 'http://hp-med-forms3.eu-2.evennode.com/api/';

  constructor(
    private http: Http
  ) { }

  // Check templates update of templates
  public getTemplatesVersion() {
    return this.http.get( this.apiUrl + 'update/templates/check')
      .map( (res)  => res.json())
      .map((res) => {
        if (res.templates) {
          return res.templates;
        }
      });
  }

  // get latest templates from server
  public getTemplates() {
    return this.http.get( this.apiUrl + 'update/templates')
      .map( (res)  => res.json())
      .map((res) => {
        if (res.templates) {
          return res.templates;
        }
      });
  }

}
