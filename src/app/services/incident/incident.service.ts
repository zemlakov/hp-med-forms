import { Injectable } from '@angular/core';
import { CurrentFormDataService } from '../current-form-data/current-form-data.service';
import { TemplateFormService } from '../template-form/template-form.service';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';

@Injectable()
export class IncidentService {

  // Incident params
  public currentIncidentID: any;            // Unique ID of Incident (Int). False if created new incident and still not saved.
  public currentIncidentName: any;          // Unique ID of Incident (Int). False if created new incident and still not saved.
  public currentIncidentStatus: any;        // New or Saved
  public currentIncidentEditable: any;      // True/false. If outdated - false

  private incidentsInstance: any;

  constructor(
    private currentForm: CurrentFormDataService,
    private template: TemplateFormService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  loadIncidents() {
    let incidentsString = localStorage.getItem('incidents');

    if (incidentsString) {
      this.incidentsInstance = JSON.parse(incidentsString);
    }else {
      this.incidentsInstance = [];
    }
  }

  getIncidentsList() {

    if (!this.incidentsInstance) {
      this.loadIncidents();
    }

    let incidentList = [];

    this.incidentsInstance.forEach(element => {
      incidentList.push(
        {
          'id': element.id,
          'name': element.name,
          'date': element.date,
          'status': this.isIncidentActual(element),
          'forms': this.checkIncidentFroms(element)
        }
      );
    });


    return incidentList;
  }

  saveNewIncident() {
    if (!this.incidentsInstance) {
      this.loadIncidents();
    }

    let incidentItem = {
      'id': this.generateInstanceID(),
      'name': this.currentIncidentName,
      'date': Date.now(),
      'template_version': '',
      'forms_data': this.generateFormData()
    };
    this.incidentsInstance.push(incidentItem);

    localStorage.setItem('incidents', JSON.stringify(this.incidentsInstance));

    this.currentIncidentStatus = 'saved';
    this.currentIncidentID = incidentItem.id;

  }

  // Helper for generating new Instance ID (highest val + 1)
  generateInstanceID() {
    let idList = [];
    let id = 0;

    // If Instance not empty
    if (this.incidentsInstance && this.incidentsInstance.length > 0) {
      // Fill list of IDs
      this.incidentsInstance.forEach(incident => {
        idList.push(Number(incident.id));
      });

      // Sort array list in ascending order
      idList.sort( (a, b) => {
        return a - b ;
      });

      // Return last value + 1;
      let currentHighestID = idList[idList.length - 1];
      id = currentHighestID + 1;
    }

    return id;
  }

  // Helper for generating forms data
  generateFormData() {
    let formData = [];
    // get list of available forms in template with it's IDs and names
    let forms = this.template.getNavItems();

    // Populate formData with empty forms objects
    forms.forEach( item => {
      formData.push({'id': item.id, 'data': false});
    });

    return formData;
  }

  // Check, is the incident up to ate with current templatye and data
  isIncidentActual(item: string) {
    // .template_version
    return true;
  }

  // Fills rorms with it's statuses (For list on main)
  checkIncidentFroms(item) {

    let forms = [];

    item.forms_data.forEach(element => {
      if (element.data) {
        forms.push(
          {[element.id]: true }
        );
      }else {
        forms.push(
          {[element.id]: false }
        );
      }
    });

    return forms;

  }

  // Delete specific incident (Home page)
  deleteincident(id: Number) {
    this.incidentsInstance.forEach((element, index) => {
      if (element.id === Number(id)) {
        this.incidentsInstance.splice(index, 1);
      }
    });

    localStorage.setItem('incidents', JSON.stringify(this.incidentsInstance));
  }

  // Save current form to the current incident
  saveCurrentForm() {

    this.incidentsInstance.forEach( (incident, incidentIndex) => {
      if (incident.id == this.currentIncidentID) {

        this.incidentsInstance[incidentIndex].forms_data.forEach( (form, formIndex) => {
          if (form.id === this.currentForm.currentFormID) {
            this.incidentsInstance[incidentIndex].forms_data[formIndex].data = this.currentForm.currentFormData;
          }
        });
      }
    });

    localStorage.setItem('incidents', JSON.stringify(this.incidentsInstance));
  }

  // Delete current form to the current incident
  delteCurrentForm() {

    this.incidentsInstance.forEach( (incident, incidentIndex) => {
      if (incident.id == this.currentIncidentID) {

        this.incidentsInstance[incidentIndex].forms_data.forEach( (form, formIndex) => {
          if (form.id === this.currentForm.currentFormID) {
            this.incidentsInstance[incidentIndex].forms_data[formIndex].data = false;
          }
        });
      }
    });

    localStorage.setItem('incidents', JSON.stringify(this.incidentsInstance));

    // Go to home
    this.router.navigate(['/']);
  }

  // Get specific form data
  getFormData(incidentID: number, formID: string) {
    let data = {};

    this.incidentsInstance.forEach( (incident, incidentIndex) => {
      if (incident.id === incidentID) {
        this.incidentsInstance[incidentIndex].forms_data.forEach( (form, formIndex) => {
          if (form.id === formID) {
            data = this.incidentsInstance[incidentIndex].forms_data[formIndex].data;
          }
        });
      }
    });

    return data;
  }

  // Get empty forms on incident
  getEmptyForms(incidentID: number) {

    if (!this.incidentsInstance) {
      this.loadIncidents();
    }

    let formsList = [];

    this.incidentsInstance.forEach((elementIncident, incidentIndex) => {
      if (elementIncident.id == incidentID) {

        this.incidentsInstance[incidentIndex].forms_data.forEach((elementForm, formIndex) => {

          if (!elementForm.data) {
            formsList.push(
              {
                'id': elementForm.id,
                'name': this.template.getRenderFormName(elementForm.id)
              }
            );
          }
        });
      }
    });

    return formsList;

  }

  // Check empty forms on incident (For drop down 'Neus Formular')
  hasEmptyForms(incidentID: number) {

    if (!this.incidentsInstance) {
      this.loadIncidents();
    }

    let empty = true;

    this.incidentsInstance.forEach((elementIncident, incidentIndex) => {
      if (elementIncident.id == incidentID) {

        this.incidentsInstance[incidentIndex].forms_data.forEach((elementForm, formIndex) => {

          if (!elementForm.data) {
            empty = false;
          }
        });
      }
    });

    return empty;
  }

  // Get incident name (For header incident title)
  getIncidentName(incidentID: number) {
    let incidentName = 'error';

    this.incidentsInstance.forEach(incident => {
      if (incident.id == incidentID) {
        incidentName = incident.name;
      }
    });

    return incidentName;
  }

}
