import { Injectable } from '@angular/core';

@Injectable()
export class TemplateFormService {

  // General schema object which simulate "local storage" (will be repalced when storage solution will be agreed)

  private generalFormTemplateObject: any;

  constructor() { }

  // Get list of Forms ids and its names for navigation
  getNavItems() {
    if  (this.generalFormTemplateObject) {
      let navList = [];

      Object.keys(this.generalFormTemplateObject['data']).map( (objectKey) => {
        let navItem = {
          'id': objectKey,
          'title': this.generalFormTemplateObject['data'][objectKey].formName
        };

        navList.push(navItem);
      });

      return navList;
    }
  }

  // get name
  getRenderFormName(id: string) {
    return this.generalFormTemplateObject['data'][id].formName;
  }

  // get description
  getRenderFormDescription(id: string) {
    return this.generalFormTemplateObject['data'][id].formDescription;
  }

  getRenderSchema(id: string) {
    return this.generalFormTemplateObject['data'][id].renderSchema;
  }

  getRenderDataSchema(id: string) {
    return JSON.parse(JSON.stringify(this.generalFormTemplateObject['data'][id].renderDataSchema));
  }

  // Get dynamic resources refference
  getDynamicResourcesReff(id: string) {
    //return JSON.parse(JSON.stringify(this.generalFormTemplateObject['data'][id].renderDataSchema));
    let dynamicResources = [
      {
        "key": "system_name",
        "data_group": "devices",
        "data_property": "name"
      },
      {
        "key": "customer_name",
        "data_group": "customer",
        "data_property": "name"
      },
      {
        "key": "street",
        "data_group": "customer",
        "data_property": "street"
      },
      {
        "key": "city",
        "data_group": "customer",
        "data_property": "city"
      },
      {
        "key": "zip_code",
        "data_group": "customer",
        "data_property": "zip"
      },
      {
        "key": "appeal",
        "data_group": "customer",
        "data_property": "appeal"
      },
      {
        "key": "contact",
        "data_group": "customer",
        "data_property": "contact"
      }
    ];

    return dynamicResources;
  }

  // get printLayout Schema
  getPrintLayout(id: string) {
    return this.generalFormTemplateObject['data'][id].printLayout;
  }

  // get PrintContent Schema
  getPrintContent(id: string) {
    return this.generalFormTemplateObject['data'][id].printContent;
  }

  // get current template versio
  getTemplateVersion() {
    if  (this.generalFormTemplateObject) {
      return this.generalFormTemplateObject['updatedAt'];
    }
  }

  // Save template to local sorage
  saveTemplate(tpl: string) {
    localStorage.setItem('currentTemplate', tpl);
    console.log('Current templates updated!');
  }

  // Load current template from local storage to app scope
  loadCurrentTemplate() {
    let tpl = JSON.parse(localStorage.getItem('currentTemplate'));

    if (tpl) {
      // Parse strings in tpl object
      Object.keys(tpl['data']).map( (key) => {
        //console.log(tpl['data'][key]['renderDataSchema']);

        // printContent
        tpl['data'][key].printContent = JSON.parse(tpl['data'][key].printContent);
        // printLayout
        tpl['data'][key]['printLayout'] = JSON.parse(tpl['data'][key]['printLayout']);
        // renderDataSchema
        tpl['data'][key]['renderDataSchema'] = JSON.parse(tpl['data'][key]['renderDataSchema']);
        // renderDataSchema
        tpl['data'][key]['renderSchema'] = this.b64DecodeUnicode(tpl['data'][key]['renderSchema']);
      });
    }

    this.generalFormTemplateObject = tpl;

  }

  public b64DecodeUnicode(str: string): string {
        if (window
            && "atob" in window
            && "decodeURIComponent" in window) {
            return decodeURIComponent(Array.prototype.map.call(atob(str), (c) => {
                return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(""));
        } else {
            console.warn("b64DecodeUnicode requirements not acepted");
            return null;
        }
    }

}
