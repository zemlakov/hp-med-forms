import { Component, ComponentRef, ViewChild, ViewContainerRef} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AfterViewInit, OnInit, OnDestroy} from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd  } from '@angular/router';
import { OnChanges, ComponentFactory} from '@angular/core';

import { IncidentService } from '../services/incident/incident.service';
import { CurrentFormDataService } from '../services/current-form-data/current-form-data.service';
import { TemplateFormService } from '../services/template-form/template-form.service';

import { FormDynamicData, DynamicTypeBuilder } from './form.builder';

@Component({
  selector: 'app-form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.scss']
})
export class FormTemplateComponent implements OnInit {
  // reference for the place where generated component will be placed (dynamicComponentTarget)
  @ViewChild('formTpl', {read: ViewContainerRef})
  public dynamicComponentTarget: ViewContainerRef;

  // this will be reference to dynamic content - to be able to destroy it
  protected componentRef: ComponentRef<FormDynamicData>;

  // until ngAfterViewInit, we cannot start (firstly) to process dynamic stuff
  protected wasViewInitialized = false;


  // Render form properties
  schema: any = null;
  currentFormData: any = {};

  // Initial form meta
  name: string;
  description: string;

  // Observebles handlers
  routerHandler: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public incident: IncidentService,
    public currentFormSevice: CurrentFormDataService,
    public formTempalte: TemplateFormService,
    public viewContainerRef: ViewContainerRef,
    protected typeBuilder: DynamicTypeBuilder
  ) {}

  ngOnInit() {
      // Watch on app state if
      this.routerHandler = this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          if (this.componentRef && this.wasViewInitialized) {
            console.log('current id:  ' + this.currentFormSevice.currentFormID);
            this.setInitialData();
            this.refreshContent();
          }
        }
      });
  }

  ngAfterViewInit() {
    this.setInitialData();
    this.wasViewInitialized = true;
    this.refreshContent();
  }

  // wasViewInitialized is an IMPORTANT switch
  // when this component would have its own changing @Input()
  // - then we have to wait till view is intialized - first OnChange is too soon
  /*ngOnChanges() {

    if (this.wasViewInitialized) {
      return;
    }
    this.refreshContent();
  }*/

  refreshContent() {

      if (this.componentRef) {
          this.componentRef.destroy();
      }

      // here we get a TEMPLATE with dynamic content === TODO
      let template = this.schema;

      // here we get Factory (just compiled or from cache)
      this.typeBuilder
          .createComponentFactory(template)
          .then((factory: ComponentFactory<FormDynamicData>) => {
            // Target will instantiate and inject component (we'll keep reference to it)
            this.componentRef = this
                .dynamicComponentTarget
                .createComponent(factory);

            // let's inject @Inputs to component instance
            let component = this.componentRef.instance;

            component.entity = this.currentFormData;
           });
  }

  ngOnDestroy() {

    // prevent memory leak when component destroyed
    this.routerHandler.unsubscribe();

    if (this.componentRef) {
      this.componentRef.destroy();
      this.componentRef = null;
    }

    this.wasViewInitialized = false;
  }

  setInitialData() {

    let id = this.currentFormSevice.currentFormID;

    if (this.currentFormSevice.currentFormStatus !== 'saved'){
      id = this.route.snapshot.url[1].path;
      // Set current form id
      this.currentFormSevice.setFormId(id);
    }

    this.schema =  this.formTempalte.getRenderSchema(id);

    // Fill with initial current form data (Forn new- fron renderDataSchema, for saved - from currentFormData in service)
    if (this.currentFormSevice.currentFormStatus === 'new') {
      this.currentFormData = this.formTempalte.getRenderDataSchema(id);
      console.log('Fill with render schema!');
    } else {
      console.log('Fill with service data!');
      this.currentFormData = this.currentFormSevice.currentFormData;
    }

    this.name = this.formTempalte.getRenderFormName(id);
    this.description = this.formTempalte.getRenderFormDescription(id);
  }

}
