import { Component, ComponentFactory, NgModule, Input, Injectable, Compiler} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewChild} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { AfterViewInit, OnInit, OnDestroy} from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JitCompilerFactory } from '@angular/compiler';

import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';

import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';

import { CurrentFormDataService } from '../services/current-form-data/current-form-data.service';
import { CustomerDataService } from '../services/customer-data/customer-data.service';

import { IncidentService } from '../services/incident/incident.service';
import { TemplateFormService } from '../services/template-form/template-form.service';


export interface FormDynamicData {
    entity: any;
}

@Injectable()
export class DynamicTypeBuilder {

    private compiler: Compiler = new JitCompilerFactory([{useDebug: true, useJit: true}]).createCompiler();

    private _cacheOfFactories: {[templateKey: string]: ComponentFactory<FormDynamicData>} = {};

    constructor() {}

    public createComponentFactory(template: string): Promise<ComponentFactory<FormDynamicData>> {

        let factory = this._cacheOfFactories[template];

        if (factory) {
            console.log('Module and Type are returned from cache');

            return new Promise((resolve) => {
                resolve(factory);
            });
        }

        // unknown template ... let's create a Type for it
        let type   = this.createNewComponent(template);
        let module = this.createComponentModule(type);

        return new Promise((resolve) => {
            this.compiler
                .compileModuleAndAllComponentsAsync(module)
                .then((moduleWithFactories) => {
                    factory = _.find(moduleWithFactories.componentFactories, { componentType: type });

                    this._cacheOfFactories[template] = factory;

                    resolve(factory);
                });
        });
    }

    protected createNewComponent (tmpl: string) {
        @Component({
            selector: 'dynamic-component',
            template: tmpl
        })
        class CustomDynamicComponent  implements FormDynamicData {
            @Input()  public entity: any;
            @ViewChild('f') public dynForm: any;

            // Observebles handlers
            formStateHandler: any;
            currentDatahandler: any;

            dynamicData: any = {};

            constructor(
                public currentFormSevice: CurrentFormDataService,
                public customerData: CustomerDataService,
                public formTempalte: TemplateFormService,
                private route: ActivatedRoute,
                private router: Router,
            ) {
                // Subscribe on form state in  CurrentFormservice
                this.formStateHandler = this.currentFormSevice.formState$.subscribe(
                    state => {
                        console.log('submit btn activated!: ');
                        if (this.dynForm) {
                            this.dynForm.onSubmit(true);
                        }else {
                            console.log('No form "#f" defined in template or template is broken.\nCheck render schema for this template.');
                        }
                    }
                );
            }

            ngOnInit() {
                this.setInitialData();
            }

            ngAfterViewInit() {
                // Subscribe on entity and set in currentForm service
                if (this.dynForm) {
                    this.currentDatahandler = this.dynForm.control.valueChanges.subscribe(data => {
                        if (data) {
                            this.currentFormSevice.setFormData(this.entity);
                        }
                    });
                }else {
                    console.log('No form "#f" defined in template or template is broken.\nCheck render schema for this template.');                    
                }
            }

            ngOnDestroy() {
                // prevent memory leak when component destroyed
                if (this.currentDatahandler) {
                    this.currentDatahandler.unsubscribe();
                }
                if (this.formStateHandler) {
                    this.formStateHandler.unsubscribe();
                }
                if (this.dynForm) {
                    this.dynForm = null;
                }
            }

            setInitialData() {
                let id = this.currentFormSevice.currentFormID;
                if (this.currentFormSevice.currentFormStatus !== 'saved') {
                    id = this.route.snapshot.url[1].path;
                    // Set current form id
                    this.currentFormSevice.setFormId(id);
                }
                /** Load data sources of dynamic data for typehead input fields  */
                let dynamicDataReff = this.formTempalte.getDynamicResourcesReff(id);
                this.dynamicData = this.initDynamicData(dynamicDataReff);
            }

            // Set up Observables for typeheads object in dynamicResources in template
            initDynamicData(dataRef: any) {

                let refObject = {};

                dataRef.forEach(element => {

                if (element.data_group === 'customer') {
                    refObject[element.key] = Observable
                    .create((observer: any) => {
                        observer.next(this.entity.customer_data.customer[element.key]);
                    })
                    .mergeMap((token: string) => this.customerData.getCustomerData(token, element.data_property));
                }
                if (element.data_group === 'devices') {
                    refObject[element.key] = Observable
                    .create((observer: any) => {
                        observer.next(this.entity.over_the_device[element.key]);
                    })
                    .mergeMap((token: string) => this.customerData.getDeviceData(token, element.data_property));
                }

                });

                return refObject;
            }

            isValid(form, parameter) {
                if (form.form.controls[parameter]) {
                    if (form.submitted && !form.form.controls[parameter].valid && !form.form.controls[parameter].disabled) {
                        return false;
                    }else {
                        return true;
                    }
                }
            }

            _onSubmit(f) {
                console.log('Form valid: ' + f.valid);
                this.currentFormSevice.currentFormValid = f.valid;
            }
        };

        // a component for this particular template
        return CustomDynamicComponent;
    }

    protected createComponentModule (componentType: any) {
        @NgModule({
            declarations: [
            componentType
            ],
            imports: [CommonModule, ReactiveFormsModule, FormsModule, TypeaheadModule.forRoot(), NKDatetimeModule ],
            exports: [CommonModule]
        })
        class RuntimeComponentModule {}
        // a module for just this Type
        return RuntimeComponentModule;
    }
}